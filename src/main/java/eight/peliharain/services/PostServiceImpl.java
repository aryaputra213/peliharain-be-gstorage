package eight.peliharain.services;

import eight.peliharain.model.PenggunaModel;
import eight.peliharain.model.PostModel;
import eight.peliharain.model.StatusAdopsi;
import eight.peliharain.model.UserModelRole;
import eight.peliharain.repository.PenggunaModelRepository;
import eight.peliharain.repository.PostModelRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Set;
import java.util.stream.Collectors;

@Service
public class PostServiceImpl implements PostService {

    @Autowired
    private PostModelRepository postModelRepository;

    @Autowired
    private PenggunaModelRepository penggunaModelRepository;

    @Override
    public PostModel createPost(String username, PostModel postModel) {
        var queryResult = penggunaModelRepository.findByUsername(username);

        if (queryResult.isPresent()) {
            PenggunaModel penggunaModel = queryResult.get();

            postModel.setPenggunaModel(penggunaModel);
            postModel.setStatus(StatusAdopsi.MENUNGGU_VERIFIKASI);
            penggunaModel.getPostModelList().add(postModel);

            postModelRepository.save(postModel);
            penggunaModelRepository.save(penggunaModel);

            return postModel;
        }

        return null;
    }

    @Override
    public Iterable<PostModel> getAllPost() {
        return postModelRepository.findAll().stream()
                .filter(post -> post.getStatus() == StatusAdopsi.AKTIF)
                .collect(Collectors.toList());
    }

    @Override
    public PostModel getPostById(long id) {
        var queryResult = postModelRepository.findById(id);
        return queryResult.orElse(null);
    }

    @Override
    public Iterable<PostModel> getPostsFromUser(String username) {
        var queryResult = penggunaModelRepository.findByUsername(username);

        if (queryResult.isPresent()) {
            PenggunaModel penggunaModel = queryResult.get();
            return penggunaModel.getPostModelList();
        }

        return null;
    }

    @Override
    public PostModel markAdoptedPost(String username, long id) throws IllegalAccessException {
        var queryResultPost = postModelRepository.findById(id);
        var queryResultPengguna = penggunaModelRepository.findByUsername(username);

        if (queryResultPost.isPresent() && queryResultPengguna.isPresent()) {
            PostModel postModel = queryResultPost.get();
            PenggunaModel penggunaModel = queryResultPengguna.get();

            if (postModel.getPenggunaModel() == penggunaModel) {
                postModel.setStatus(StatusAdopsi.TERADOPSI);
                deletePost(id);
                return postModelRepository.save(postModel);
            }

            throw new IllegalAccessException("The user does not own the Adoption Post");
        }

        return null;
    }

    @Override
    public Iterable<PostModel> getAwaitingConfirmationPosts(String username) throws IllegalAccessException {
        var queryResult =  penggunaModelRepository.findByUsername(username);

        if (queryResult.isPresent()) {
            PenggunaModel penggunaModel = queryResult.get();

            if (penggunaModel.getUserRole() == UserModelRole.ADMIN) {
                return postModelRepository.findAll().stream()
                        .filter(post -> post.getStatus() == StatusAdopsi.MENUNGGU_VERIFIKASI)
                        .collect(Collectors.toList());
            }

            throw new IllegalAccessException("The user is not an Admin");
        }

        return null;
    }

    @Override
    public PostModel verifyPost(String username, long id) throws IllegalAccessException {
        var queryResultPost = postModelRepository.findById(id);
        var queryResultPengguna = penggunaModelRepository.findByUsername(username);

        if (queryResultPost.isPresent() && queryResultPengguna.isPresent()) {
            PostModel postModel = queryResultPost.get();
            PenggunaModel penggunaModel = queryResultPengguna.get();

            if (penggunaModel.getUserRole() == UserModelRole.ADMIN) {
                postModel.setStatus(StatusAdopsi.AKTIF);
                return postModelRepository.save(postModel);
            }

            throw new IllegalAccessException("The user is not an Admin");
        }

        return null;
    }

    @Override
    public PostModel denyPost(String username, long id) throws IllegalAccessException {
        var queryResultPost = postModelRepository.findById(id);
        var queryResultPengguna = penggunaModelRepository.findByUsername(username);

        if (queryResultPost.isPresent() && queryResultPengguna.isPresent()) {
            PostModel postModel = queryResultPost.get();
            PenggunaModel penggunaModel = queryResultPengguna.get();

            if (penggunaModel.getUserRole() == UserModelRole.ADMIN) {
                postModel.setStatus(StatusAdopsi.GAGAL_VERIFIKASI);
                deletePost(id);
                return postModelRepository.save(postModel);
            }

            throw new IllegalAccessException("The user is not an Admin");
        }

        return null;
    }

    private void deletePost(long id) {
        new java.util.Timer().schedule(
                new java.util.TimerTask() {
                    @Override
                    public void run() {
                        postModelRepository.deleteById(id);
                    }
                },
                60000 // 1 minute
        );
    }

    // Bookmark Section start

    @Override
    public Boolean isBookmarked(String username, long idPost) throws IllegalAccessException {
        var queryResultPost = postModelRepository.findById(idPost);
        var queryResultPengguna = penggunaModelRepository.findByUsername(username);

        if (queryResultPost.isPresent() && queryResultPengguna.isPresent()) {

            PenggunaModel penggunaModel = queryResultPengguna.get();
            PostModel postModel = queryResultPost.get();

            return(penggunaModel.getBookmarks().contains(postModel));
        }
        throw new IllegalAccessException("Error");
    }

    @Override
    public PostModel addBookmark(String username, long idPost) throws IllegalAccessException {
        var queryResultPost = postModelRepository.findById(idPost);
        var queryResultPengguna = penggunaModelRepository.findByUsername(username);

        if (queryResultPost.isPresent() && queryResultPengguna.isPresent()) {
            PostModel postModel = queryResultPost.get();
            PenggunaModel penggunaModel = queryResultPengguna.get();

            if (penggunaModel != postModel.getPenggunaModel()) {
                penggunaModel.getBookmarks().add(postModel);
                penggunaModelRepository.save(penggunaModel);
                // postModel.getBooked_username().add(penggunaModel);
                // postModelRepository.save(postModel);
                // System.out.println("testtest======================");
                return postModel;
            }

            throw new IllegalAccessException("The user is the author!");
        }

        return null;
    }

    @Override
    public PostModel removeBookmark(String username, long idPost) throws IllegalAccessException {
        var queryResultPost = postModelRepository.findById(idPost);
        var queryResultPengguna = penggunaModelRepository.findByUsername(username);

        if (queryResultPost.isPresent() && queryResultPengguna.isPresent()) {
            PostModel postModel = queryResultPost.get();
            PenggunaModel penggunaModel = queryResultPengguna.get();

            if (penggunaModel != postModel.getPenggunaModel()) {
                penggunaModel.getBookmarks().remove(postModel);
                penggunaModelRepository.save(penggunaModel);
                return postModel;
            }

            throw new IllegalAccessException("The user is the author!");
        }

        return null;
    }

    @Override
    public Set<PostModel> getBookmarks(String username) throws IllegalAccessException {
        var queryResultPengguna = penggunaModelRepository.findByUsername(username);

        if (queryResultPengguna.isPresent()) {
            PenggunaModel penggunaModel = queryResultPengguna.get();

            return penggunaModel.getBookmarks();
        }

        return null;
    }
}
