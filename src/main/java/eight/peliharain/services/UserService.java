package eight.peliharain.services;

import eight.peliharain.model.UserModel;

public interface UserService {
    UserModel registerUser(UserModel user);
    UserModel getUserByEmail(String email);
    public UserModel updateDataUser(long idUser, UserModel userModel);
}
