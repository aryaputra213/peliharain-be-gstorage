package eight.peliharain.services;

import eight.peliharain.model.ProdukModel;
import eight.peliharain.model.RekeningPembayaranModel;
import eight.peliharain.model.TokoModel;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

public interface TokoService {
    // Method untuk Toko
    TokoModel createToko(TokoModel tokoModel, String usernamePengguna);
    TokoModel updateToko(TokoModel tokoModel, long id);
    TokoModel getTokoById(long idToko);
    String deleteToko(long idToko);

    // Method untuk Produk
    ProdukModel getProdukById(long idProduk);
    List<ProdukModel> getAllProdukModel();
    ProdukModel addProduk(MultipartFile file, ProdukModel produkModel, long idToko) throws IOException;
    ProdukModel updateProduk(ProdukModel produkModel, long idProduk);
    String deleteProduk(long idProduk);
    public List<ProdukModel> sortProduk(long kode, long idToko);

    // Method untuk RekeningPembayaran
    RekeningPembayaranModel createRekening(RekeningPembayaranModel rekeningPembayaranModel, long idToko);
    RekeningPembayaranModel updateRekening(RekeningPembayaranModel rekeningPembayaranModel, long idRekening);
    RekeningPembayaranModel getRekeningByNoRekening(long idRekening);
    List<RekeningPembayaranModel> getAllRekening();
    String deleteRekening(long idRekening);
}
