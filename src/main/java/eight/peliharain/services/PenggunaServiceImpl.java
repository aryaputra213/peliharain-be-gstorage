package eight.peliharain.services;

import eight.peliharain.model.PenggunaModel;
import eight.peliharain.repository.PenggunaModelRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PenggunaServiceImpl implements PenggunaService {

    @Autowired
    private PenggunaModelRepository penggunaModelRepository;

    @Override
    public PenggunaModel getPenggunaByUsername(String username){
        var dataPengguna = penggunaModelRepository.findByUsername(username);
        return dataPengguna.orElse(null);
    }

    @Override
    public PenggunaModel updateDataPengguna(String username, PenggunaModel penggunaModel) {
        var dataLama = penggunaModelRepository.findByUsername(username);
        if (dataLama.isPresent()) {
            dataLama.get().setNamaLengkap(penggunaModel.getNamaLengkap());
            dataLama.get().setSocialMedia(penggunaModel.getSocialMedia());
            dataLama.get().setUserRole(penggunaModel.getUserRole());
            penggunaModelRepository.save(dataLama.get());
            return dataLama.get();
        } else {
            return null;
        }
    }

}
