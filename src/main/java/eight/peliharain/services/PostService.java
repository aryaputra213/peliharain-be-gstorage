package eight.peliharain.services;

import java.util.Set;

import eight.peliharain.model.PostModel;

public interface PostService {

    PostModel createPost(String username, PostModel postModel);

    Iterable<PostModel> getAllPost();

    PostModel getPostById(long id);

    Iterable<PostModel> getPostsFromUser(String username);

    PostModel markAdoptedPost(String username, long id) throws IllegalAccessException;

    Iterable<PostModel> getAwaitingConfirmationPosts(String username) throws IllegalAccessException;

    PostModel verifyPost(String username, long id) throws IllegalAccessException;

    PostModel denyPost(String username, long id) throws IllegalAccessException;

    Boolean isBookmarked(String username, long idPost) throws IllegalAccessException;

    PostModel addBookmark(String username, long idPost) throws IllegalAccessException;

    PostModel removeBookmark(String username, long idPost) throws IllegalAccessException;

    Set<PostModel> getBookmarks(String username) throws IllegalAccessException;
}
