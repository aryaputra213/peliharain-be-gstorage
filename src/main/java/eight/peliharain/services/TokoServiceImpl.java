package eight.peliharain.services;

import eight.peliharain.model.ProdukModel;
import eight.peliharain.model.RekeningPembayaranModel;
import eight.peliharain.model.TokoModel;
import eight.peliharain.model.UserModelRole;
import eight.peliharain.repository.PenggunaModelRepository;
import eight.peliharain.repository.ProdukModelRepository;
import eight.peliharain.repository.RekeningPembayaranModelRepository;
import eight.peliharain.repository.TokoModelRepository;
import eight.peliharain.utils.HargaSorter;
import eight.peliharain.utils.Sorter;
import eight.peliharain.utils.StokSorter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Service
public class TokoServiceImpl implements TokoService{

    @Autowired
    private TokoModelRepository tokoModelRepository;

    @Autowired
    private ProdukModelRepository produkModelRepository;

    @Autowired
    private PenggunaModelRepository penggunaModelRepository;

    @Autowired
    private RekeningPembayaranModelRepository rekeningPembayaranModelRepository;

    @Autowired
    private GoogleFileRepoServiceImpl googleFileRepoServiceImpl;

    // Method untuk Toko
    @Override
    public TokoModel createToko(TokoModel tokoModel, String usernamePengguna) {
        var dataUser = penggunaModelRepository.findByUsername(usernamePengguna);

        if (dataUser.isPresent()) {
            dataUser.get().setToko(tokoModel);
            dataUser.get().setUserRole(UserModelRole.PEMILIKTOKO);
            tokoModelRepository.save(tokoModel);

            return tokoModel;
        }
        return null;
    }

    @Override
    public TokoModel updateToko(TokoModel tokoModel, long idToko) {
        var dataToko = tokoModelRepository.findById(idToko);

        if (dataToko == null) {
            return null;
        } else {
            dataToko.setNamaToko(tokoModel.getNamaToko());
            dataToko.setDeskripsiToko(tokoModel.getDeskripsiToko());
            dataToko.setPhoto(tokoModel.getPhoto());
            dataToko.setAlamat(tokoModel.getAlamat());
            tokoModelRepository.save(dataToko);
            return dataToko;
        }
    }

    @Override
    public TokoModel getTokoById(long idToko) {
        var dataToko = tokoModelRepository.findById(idToko);
        if (dataToko != null) {
            return dataToko;
        } else {
            return null;
        }
    }

    @Override
    public String deleteToko(long idToko) {
        var dataToko = tokoModelRepository.findById(idToko);

        if (dataToko != null) {
            tokoModelRepository.deleteById(idToko);
            return "Berhasil Menghapus";
        }
        return "Gagal Menghapus";
    }

    // Method untuk Produk
    @Override
    public ProdukModel getProdukById(long idProduk) {
        var dataProduk = produkModelRepository.findById(idProduk);
        return dataProduk.orElse(null);
    }

    @Override
    public List<ProdukModel> getAllProdukModel() {
        var allProduk = produkModelRepository.findAll();
        return allProduk;
    }

    @Override
    public ProdukModel addProduk(MultipartFile file, ProdukModel produkModel, long idToko) throws IOException {
        var dataToko = tokoModelRepository.findById(idToko);
        System.out.println("TOKO");
       if (dataToko != null) {
           produkModel.setFoto(googleFileRepoServiceImpl.uploadFile(file));
           produkModel.setId(idToko);
           produkModel.setTokoModel(dataToko);
           Set<ProdukModel> produkModelSet = dataToko.getProdukModelSet();
           produkModelSet.add(produkModel);
           dataToko.setProdukModelSet(produkModelSet);
           produkModelRepository.save(produkModel);

           return produkModel;
       }

        return null;
    }

    @Override
    public ProdukModel updateProduk(ProdukModel produkModelBaru, long idProduk) {
        var dataLama = produkModelRepository.findById(idProduk);
        if (dataLama.isPresent()) {
            dataLama.get().setNamaProduk(produkModelBaru.getNamaProduk());
            dataLama.get().setDeskripsiProduk(produkModelBaru.getDeskripsiProduk());
            dataLama.get().setFoto(produkModelBaru.getFoto());
            dataLama.get().setStok(produkModelBaru.getStok());
            dataLama.get().setHarga(produkModelBaru.getHarga());
            produkModelRepository.save(dataLama.get());
            return dataLama.get();
        } else {
            return null;
        }
    }

    @Override
    public String deleteProduk(long idProduk) {
        if (produkModelRepository.findById(idProduk).isPresent()) {
            produkModelRepository.deleteById(idProduk);
            return "Berhasil Dihapus";
        }
        return "Gagal Dihapus";
    }

    @Override
    public List<ProdukModel> sortProduk(long kode,long idToko) {
        if (idToko != -1) {
            TokoModel dataToko = tokoModelRepository.findById(idToko);
            List<ProdukModel> produkList = new ArrayList<>();
            for (ProdukModel pm : dataToko.getProdukModelSet()) {
                produkList.add(pm);
            }
            if (kode == 1) {
                Sorter stokSorter = new StokSorter();
                return stokSorter.filterAction(produkList);
            } else {
                Sorter hargaSorter = new HargaSorter();
                return hargaSorter.filterAction(produkList);
            }
        } else {
            if (kode == 1) {
                Sorter stokSorter = new StokSorter();
                return stokSorter.filterAction(produkModelRepository.findAll());
            } else {
                Sorter hargaSorter = new HargaSorter();
                return hargaSorter.filterAction(produkModelRepository.findAll());
            }
        }
    }
    // Checkme: pastiin lagi id buat rekening itu norek apa beda sendiri
    // Method untuk RekeningPembayaran
    @Override
    public RekeningPembayaranModel createRekening(RekeningPembayaranModel rekeningPembayaranModel, long idToko) {
        var dataToko = tokoModelRepository.findById(idToko);

        if (dataToko != null) {
            rekeningPembayaranModel.setTokoModel(dataToko);
            Set<RekeningPembayaranModel> rekeningPembayaranModelSet = dataToko.getRekeningPembayaranModelSet();
            rekeningPembayaranModelSet.add(rekeningPembayaranModel);
            dataToko.setRekeningPembayaranModelSet(rekeningPembayaranModelSet);
            rekeningPembayaranModelRepository.save(rekeningPembayaranModel);

            return rekeningPembayaranModel;
        }
        return null;
    }

    @Override
    public RekeningPembayaranModel updateRekening(RekeningPembayaranModel rekeningPembayaranModelBaru, long idRekening) {
        var dataRekeningLama = rekeningPembayaranModelRepository.findById(idRekening);

        if (dataRekeningLama.isPresent()) {
            dataRekeningLama.get().setQrCode(rekeningPembayaranModelBaru.getQrCode());
            return dataRekeningLama.get();
        }
        return null;
    }

    @Override
    public RekeningPembayaranModel getRekeningByNoRekening(long idRekening) {
        var dataRekeningPembayaran = rekeningPembayaranModelRepository.findById(idRekening);
        return dataRekeningPembayaran.orElse(null);
    }

    @Override
    public List<RekeningPembayaranModel> getAllRekening() {
        return rekeningPembayaranModelRepository.findAll();
    }

    @Override
    public String deleteRekening(long idRekening) {
        var dataRekeningPembayaran = rekeningPembayaranModelRepository.findById(idRekening);

        if (dataRekeningPembayaran.isPresent()) {
            rekeningPembayaranModelRepository.deleteById(idRekening);

            return "Berhasil Menghapus";
        }
        return "Gagal Menghapus";
    }
}
