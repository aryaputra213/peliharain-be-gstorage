package eight.peliharain.services;

import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

public interface GoogleFileRepoService {
    String uploadFile(MultipartFile file) throws IOException;
}
