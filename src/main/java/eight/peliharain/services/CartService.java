package eight.peliharain.services;

import java.util.ArrayList;
import java.util.Map;
import java.util.Set;

import eight.peliharain.model.CartProductQuantity;
import eight.peliharain.model.CartProductQuantityID;
import eight.peliharain.model.CheckoutModel;
import eight.peliharain.model.CheckoutProductQuantity;
import eight.peliharain.model.ProdukModel;

public interface CartService {

    ArrayList<Map<Object, Object>> getProduk(String username);

    CartProductQuantity addProdukToCart(String username, long produkId);

    void removeProdukFromCart(String username, long produkId);

    Integer setQuantityProduk(String username, int quantity, CartProductQuantityID cartProductQuantityID);

    CheckoutModel createCheckoutFromCart(String username, String deskripsi,
            ArrayList<CartProductQuantityID> cartProductQuantities);

    Set<CheckoutModel> getCheckout(String username);

    long getCartId(String username);
}
