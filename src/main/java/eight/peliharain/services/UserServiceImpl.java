package eight.peliharain.services;

import eight.peliharain.model.UserModel;
import eight.peliharain.repository.UserModelRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserModelRepository userRepository;

    @Override
    public UserModel registerUser(UserModel user) {
        return userRepository.save(user);
    }

    @Override
    public UserModel getUserByEmail(String email) {
        var dataUser = userRepository.findByEmail(email);
        return dataUser.orElse(null);
    }

    @Override
    public UserModel updateDataUser(long idUser, UserModel userModel) {
        var dataUserLama = userRepository.findById(idUser);
        if (dataUserLama.isPresent()) {
            dataUserLama.get().setEmail(userModel.getEmail());
            userRepository.save(dataUserLama.get());
            return dataUserLama.get();
        } else {
            return null;
        }

    }
}
