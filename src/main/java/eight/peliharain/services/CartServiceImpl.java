package eight.peliharain.services;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import eight.peliharain.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import eight.peliharain.model.CartProductQuantity;
import eight.peliharain.model.CartProductQuantityID;
import eight.peliharain.model.CheckoutModel;
import eight.peliharain.model.CheckoutProductQuantity;
import eight.peliharain.model.PenggunaModel;
import eight.peliharain.model.ProdukModel;
import eight.peliharain.model.StatusCheckout;
import eight.peliharain.model.TokoModel;
import eight.peliharain.model.UserModel;

@Service
public class CartServiceImpl implements CartService {

    @Autowired
    private PenggunaModelRepository penggunaModelRepository;

    @Autowired
    private ProdukModelRepository produkModelRepository;

    @Autowired
    private CartProductQuantityRepository cartProductQuantityRepository;

    @Autowired
    private CheckoutModelRepository checkoutModelRepository;

    @Autowired
    private TokoModelRepository tokoModelRepository;

    @Autowired
    private CheckoutProductQuantityRepository checkoutProductQuantityRepository;

    private Map<Object, Object> BuildProdukMap(long id, String name, String img, int price, int quantity, int stok) {
        Map<Object, Object> retObj = new HashMap<>();
        retObj.put("id", id);
        retObj.put("name", name);
        retObj.put("img", img);
        retObj.put("price", price);
        retObj.put("quantity", quantity);
        retObj.put("stok", stok);
        return retObj;
    }

    private Map<Object, Object> BuildStoreMap(long id, String name, String img, String addr,
            ArrayList<Map<Object, Object>> items) {
        Map<Object, Object> retObj = new HashMap<>();
        retObj.put("id", id);
        retObj.put("name", name);
        retObj.put("img", img);
        retObj.put("addr", addr);
        retObj.put("items", items);
        return retObj;
    }

    @Override
    public ArrayList<Map<Object, Object>> getProduk(String username) {
        var queryResultPengguna = penggunaModelRepository.findByUsername(username);

        if (queryResultPengguna.isPresent()) {
            PenggunaModel pengguna = queryResultPengguna.get();
            ArrayList<Map<Object, Object>> returnItem = new ArrayList<>();

            Set<CartProductQuantity> items = pengguna.getCart().getCartProductQuantities();
            Iterator<CartProductQuantity> itr = items.iterator();
            CartProductQuantity tmp;
            ProdukModel tmpProduk;
            Map<Object, Object> tmpMap;
            ArrayList<Map<Object, Object>> tmpArray;
            boolean paramIsTokoAdded;
            while (itr.hasNext()) {
                tmp = itr.next();
                tmpProduk = tmp.getProduk();
                paramIsTokoAdded = false;
                // check if store is in returnItem already
                for (Object item : returnItem) {
                    tmpMap = (Map<Object, Object>) item;
                    if (tmpMap.get("id").equals((Object) tmpProduk.getTokoModel().getId())) {
                        paramIsTokoAdded = true;
                        tmpArray = (ArrayList) tmpMap.get("items");
                        tmpArray.add(BuildProdukMap(tmpProduk.getId(),
                                tmpProduk.getNamaProduk(), tmpProduk.getFoto(),
                                tmpProduk.getHarga(), tmp.getQuantity(), tmpProduk.getStok()));
                        break;
                    }
                }
                if (paramIsTokoAdded == false) {
                    tmpArray = new ArrayList<Map<Object, Object>>();
                    tmpArray.add(BuildProdukMap(tmpProduk.getId(),
                            tmpProduk.getNamaProduk(), tmpProduk.getFoto(),
                            tmpProduk.getHarga(), tmp.getQuantity(), tmpProduk.getStok()));
                    returnItem.add(BuildStoreMap(tmpProduk.getTokoModel().getId(),
                            tmpProduk.getTokoModel().getNamaToko(), tmpProduk.getTokoModel().getPhoto(),
                            tmpProduk.getTokoModel().getAlamat().getAlamat(), tmpArray));
                }
            }

            return returnItem;
        }

        return null;
    }

    @Override
    public CartProductQuantity addProdukToCart(String username, long produkId) {
        var queryResultPengguna = penggunaModelRepository.findByUsername(username);
        if (queryResultPengguna.isPresent()) {
            ProdukModel produk = produkModelRepository.findById(produkId).get();
            PenggunaModel pengguna = queryResultPengguna.get();

            CartProductQuantity new_item = new CartProductQuantity(pengguna.getCart(), produk, (Integer) 1);
            cartProductQuantityRepository.save(new_item);

            return new_item;
        }
        return null;
    }

    @Override
    public void removeProdukFromCart(String username, long produkId) {
        var queryResultPengguna = penggunaModelRepository.findByUsername(username);
        if (queryResultPengguna.isPresent()) {
            PenggunaModel pengguna = queryResultPengguna.get();
            Set<CartProductQuantity> cartItems = pengguna.getCart().getCartProductQuantities();
            Iterator<CartProductQuantity> itr = cartItems.iterator();
            CartProductQuantity tmp;
            while (itr.hasNext()) {
                tmp = itr.next();
                if (tmp.getProduk().getId() == produkId) {
                    // CartProductQuantity item =
                    pengguna.getCart().getCartProductQuantities().remove(
                            cartProductQuantityRepository.findById(tmp.getId()).get());
                    cartProductQuantityRepository.deleteById(tmp.getId());
                }
            }
        }
    }

    @Override
    public Integer setQuantityProduk(String username, int quantity, CartProductQuantityID cartProductQuantityID) {
        var queryResultPengguna = penggunaModelRepository.findByUsername(username);
        if (queryResultPengguna.isPresent()) {
            CartProductQuantity cartProductQuantity = cartProductQuantityRepository.findById(cartProductQuantityID)
                    .get();
            cartProductQuantity.setQuantity(quantity);
            cartProductQuantityRepository.save(cartProductQuantity);
            return quantity;
        }
        return null;
    }

    @Override
    public CheckoutModel createCheckoutFromCart(String username, String deskripsi,
            ArrayList<CartProductQuantityID> cartProductQuantitiesID) {

        var queryResultPengguna = penggunaModelRepository.findByUsername(username);
        if (queryResultPengguna.isPresent()) {
            PenggunaModel pembeli = queryResultPengguna.get();

            TokoModel toko = cartProductQuantityRepository.getById(cartProductQuantitiesID.get(0)).getProduk()
                    .getTokoModel();
            StatusCheckout status = StatusCheckout.PROSES;

            CheckoutModel checkout = new CheckoutModel(pembeli, toko, status, deskripsi);
            checkout = checkoutModelRepository.save(checkout); // get the id for Product in it. So save it first and put
                                                               // it in sql
            int jumlah = 0;

            for (CartProductQuantityID item : cartProductQuantitiesID) {
                CartProductQuantity temp_item_cart = cartProductQuantityRepository.getById(item);
                checkout.getCheckoutProductQuantities().add(new CheckoutProductQuantity(checkout,
                        temp_item_cart.getProduk(), temp_item_cart.getQuantity()));
                jumlah += temp_item_cart.getProduk().getHarga() * temp_item_cart.getQuantity();

                cartProductQuantityRepository.deleteById(item);
            }

            checkout.setJumlah(jumlah);
            checkoutModelRepository.save(checkout);

            return checkout;
        }
        return null;
    }

    @Override
    public long getCartId(String username) {
        return penggunaModelRepository.findByUsername(username).get().getCart().getId();
    }

    @Override
    public Set<CheckoutModel> getCheckout(String username) {

        var queryResultPengguna = penggunaModelRepository.findByUsername(username);

        if (queryResultPengguna.isPresent()) {
            return queryResultPengguna.get().getCheckouts();
        }

        return null;
    }
}
