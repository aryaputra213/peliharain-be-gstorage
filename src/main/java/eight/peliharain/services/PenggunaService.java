package eight.peliharain.services;

import eight.peliharain.model.PenggunaModel;

public interface PenggunaService {
    PenggunaModel getPenggunaByUsername(String username);
    public PenggunaModel updateDataPengguna(String username, PenggunaModel penggunaModel);
}
