package eight.peliharain.services;

import com.google.auth.Credentials;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.storage.Acl;
import com.google.cloud.storage.BlobInfo;
import com.google.cloud.storage.Storage;
import com.google.cloud.storage.StorageOptions;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

@Service
public class GoogleFileRepoServiceImpl implements GoogleFileRepoService {
    Credentials credentials;

    {
        try {
            credentials = GoogleCredentials
                        .fromStream(new FileInputStream("C:/Key/gdrive-340105-b32526a0ad3c.json"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    Storage storage = StorageOptions.newBuilder().setCredentials(credentials)
            .setProjectId("gdrive-340105").build().getService();

    private static  String bucketName = "aryaputra_drive";


    @Override
    public String uploadFile(MultipartFile file) throws IOException {
        String filename = System.nanoTime() + file.getOriginalFilename();
        System.out.println("MASUK");
        System.out.println(credentials);
        BlobInfo blobInfo = storage.create(
                BlobInfo.newBuilder(bucketName, filename)
                        .setContentType(file.getContentType())
                        .setAcl(new ArrayList<>(
                                Arrays.asList(Acl.of(Acl.User.ofAllUsers(),
                                        Acl.Role.READER))))
                        .build(), file.getInputStream());

        return "https://storage.googleapis.com/" + bucketName + "/" + filename;
    }
}
