package eight.peliharain.services;

import eight.peliharain.model.CheckoutModel;
import eight.peliharain.model.PenggunaModel;
import eight.peliharain.model.StatusCheckout;
import eight.peliharain.model.TokoModel;
import eight.peliharain.repository.CheckoutModelRepository;
import eight.peliharain.repository.PenggunaModelRepository;
import eight.peliharain.repository.TokoModelRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class CheckoutServiceImpl implements CheckoutService{
    @Autowired
    CheckoutModelRepository checkoutModelRepository;

    @Autowired
    PenggunaModelRepository penggunaModelRepository;

    @Autowired
    TokoModelRepository tokoModelRepository;

    @Override
    public CheckoutModel getCheckoutById(Long id){
        var queryResult = checkoutModelRepository.findById(id);
        return queryResult.orElse(null);
    }
    @Override
    public Iterable<CheckoutModel> getCheckoutPembeli(String username){
        var queryResult =  penggunaModelRepository.findByUsername(username);
        if (queryResult.isPresent()) {
            PenggunaModel penggunaModel = queryResult.get();
            return penggunaModel.getCheckouts();
        }

        return null;
    }
    @Override
    public Iterable<CheckoutModel> getCheckoutToko(Long idToko){
        var queryResult =  tokoModelRepository.getById(idToko);
        return queryResult.getCheckouts();
    }
    @Override
    public CheckoutModel updateCheckout(Long id, CheckoutModel checkoutModel){
        var dataLama = checkoutModelRepository.findById(id);
        if (dataLama.isPresent()) {
            dataLama.get().setBuktiPembayaran(checkoutModel.getBuktiPembayaran());
            dataLama.get().setDeskripsi(checkoutModel.getDeskripsi());
            dataLama.get().setPembeli(checkoutModel.getPembeli());
            dataLama.get().setToko(checkoutModel.getToko());
            dataLama.get().setStatus(checkoutModel.getStatus());
            checkoutModelRepository.save(dataLama.get());
            return dataLama.get();
        } else {
            return null;
        }
    }
    @Override
    public CheckoutModel updateStatusCheckout(Long id, StatusCheckout status){
        var dataLama = checkoutModelRepository.findById(id);
        if (dataLama.isPresent()) {
            dataLama.get().setStatus(status);
            checkoutModelRepository.save(dataLama.get());
            return dataLama.get();
        } else {
            return null;
        }
    }
    @Override
    public List<CheckoutModel> getAllCheckout(){
        return checkoutModelRepository.findAll();
    }
    public CheckoutModel createCheckout(String username_pembeli, CheckoutModel checkoutModel,
                                        long id_toko){
//        var pembeli = penggunaModelRepository.findByUsername(username_pembeli);
//        var toko = tokoModelRepository.findById(id_toko);
//        if (pembeli.isPresent()) {
//            checkoutModel.setPembeli(pembeli.get());
//            checkoutModel.setStatus(StatusCheckout.PROSES);
//            checkoutModel.setToko(toko);
//
//            PenggunaModel penggunaModelPembeli = pembeli.get();
//            penggunaModelPembeli.getCheckoutAsPembeli().add(checkoutModel);
//            penggunaModelRepository.save(penggunaModelPembeli);
//
//            TokoModel tokoModel = toko;
//            tokoModel.getCheckouts().add(checkoutModel);
//            tokoModelRepository.save(tokoModel);
//            checkoutModelRepository.save(checkoutModel);
//
//            return checkoutModel;
//        }
        return null;
    }
    @Override
    public String deleteCheckout(long id) {

        if (checkoutModelRepository.findById(id).isPresent()) {
            checkoutModelRepository.deleteById(id);
            return "Berhasil Dihapus";
        }
        return "Gagal Dihapus";
    }
}
