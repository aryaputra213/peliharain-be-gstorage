package eight.peliharain.services;

import eight.peliharain.model.PenggunaModel;
import eight.peliharain.model.UserModel;
import eight.peliharain.repository.PenggunaModelRepository;
import eight.peliharain.repository.UserModelRepository;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
@Setter
public class AuthServiceImpl implements AuthService {
    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    public AuthServiceImpl(
            UserModelRepository userModelRepository,
            PasswordEncoder passwordEncoder) {
        this.userModelRepository = userModelRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @Autowired
    private UserModelRepository userModelRepository;

    @Autowired
    private PenggunaModelRepository penggunaModelRepository;

    @Override
    public PenggunaModel registerUser(PenggunaModel pengguna) {
        registrationHelper(pengguna, userModelRepository);
        return penggunaModelRepository.save(pengguna);
    }

    public void registrationHelper(UserModel model, UserModelRepository repository) {
        boolean userExist = repository.findByEmail(model.getEmail()).isPresent();

        if (userExist) {
            throw new IllegalStateException("email already taken");
        }

        String encodedPassword = passwordEncoder.encode(model.getPassword());
        model.setPassword(encodedPassword);
    }
}
