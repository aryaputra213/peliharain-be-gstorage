package eight.peliharain.services;

import eight.peliharain.model.PenggunaModel;

public interface AuthService {
    public PenggunaModel registerUser(PenggunaModel pengguna);
}
