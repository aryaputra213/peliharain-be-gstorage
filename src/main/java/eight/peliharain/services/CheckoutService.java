package eight.peliharain.services;

import eight.peliharain.model.CheckoutModel;
import eight.peliharain.model.PenggunaModel;
import eight.peliharain.model.StatusCheckout;

import java.util.List;

public interface CheckoutService {
    CheckoutModel getCheckoutById(Long id);
    CheckoutModel updateCheckout(Long id, CheckoutModel checkoutModel);
    CheckoutModel updateStatusCheckout(Long id, StatusCheckout status);
    CheckoutModel createCheckout(String username_pembeli, CheckoutModel checkoutModel,
                                 long id_toko);
    List<CheckoutModel> getAllCheckout();
    Iterable<CheckoutModel> getCheckoutPembeli(String username);
    Iterable<CheckoutModel> getCheckoutToko(Long idToko);
    String deleteCheckout(long id);
}
