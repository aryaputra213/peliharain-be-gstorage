package eight.peliharain.nonentity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Embeddable;

// Checkme: ini iseng sih nyoba bikin buat di RekeningPembayaran
@Setter
@Getter
@NoArgsConstructor
@Embeddable
public class Bank {

    private String nama;
    private String logo;

}
