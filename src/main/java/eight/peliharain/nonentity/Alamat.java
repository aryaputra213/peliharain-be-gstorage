package eight.peliharain.nonentity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Embeddable;

@Getter
@Setter
@NoArgsConstructor
@Embeddable
public class Alamat {

    private String alamat;
    private String kota;
    private String kodepos;

}