package eight.peliharain.security.utils;

import eight.peliharain.model.UserModelRole;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class JWTAuthResponse {
    private final String email;
    private final String token;
    private final UserModelRole role;
    private final String username;
    private final long id;

    @Override
    public String toString() {
        return String.format(
                "{ \"email\": \"%s\", \"token\": \"%s\", \"role\": \"%s\", \"id\": \"%d\" , \"username\": \"%s\"}",
                this.getEmail(),
                this.getToken(),
                this.getRole(),
                this.getId(),
                this.getUsername()
        );
    }
}
