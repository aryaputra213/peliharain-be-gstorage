package eight.peliharain.security.utils;

public class SecurityConstants {
    public static final String SECRET = "bismillah_lulus_rpl_2021";
    public static final long EXPIRATION_TIME = 1800000; // 30 mins
    public static final String TOKEN_PREFIX = "Bearer ";
    public static final String HEADER_STRING = "Authorization";
}

