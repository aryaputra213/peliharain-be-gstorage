package eight.peliharain.security;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.fasterxml.jackson.databind.ObjectMapper;
import eight.peliharain.model.UserModel;
import eight.peliharain.security.utils.EmailPasswordAuthRequest;
import eight.peliharain.security.utils.JWTAuthResponse;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;

import static eight.peliharain.security.utils.SecurityConstants.*;


public class JWTAuthenticationFilter extends UsernamePasswordAuthenticationFilter {

    private final AuthenticationManager authenticationManager;

    public JWTAuthenticationFilter(AuthenticationManager authenticationManager) {
        this.authenticationManager = authenticationManager;
        setFilterProcessesUrl("/auth/login"); //Auth Endpoint
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest req, HttpServletResponse res) throws AuthenticationException {
        try {
            EmailPasswordAuthRequest creds = new ObjectMapper().readValue(
              req.getInputStream(),
              EmailPasswordAuthRequest.class
            );
            return authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(
                            creds.getEmail(),
                            creds.getPassword()
                    )
            );
        } catch (IOException e) {
            throw new JWTVerificationException(e.toString());
        }
    }

    @Override
    protected void successfulAuthentication(HttpServletRequest req,
                                            HttpServletResponse res,
                                            FilterChain chain,
                                            Authentication auth) throws IOException {
        String token = JWT.create()
                .withSubject(((UserModel) auth.getPrincipal()).getEmail())
                .withClaim("role",((UserModel) auth.getPrincipal()).getUserRole().name())
                .withExpiresAt(new Date(System.currentTimeMillis()+ EXPIRATION_TIME))
                .sign(Algorithm.HMAC512(SECRET.getBytes()));

        var jwtAuthResponse = new JWTAuthResponse(
                ((UserModel) auth.getPrincipal()).getEmail(),
                token,
                ((UserModel) auth.getPrincipal()).getUserRole(),
                ((UserModel) auth.getPrincipal()).getUsername(),
                ((UserModel) auth.getPrincipal()).getId()
        );

        res.addHeader("Access-Control-Expose-Headers", "*");
        res.setContentType("application/json");
        res.setCharacterEncoding("UTF-8");
        res.getWriter().write(jwtAuthResponse.toString());
        res.getWriter().flush();
    };
}
