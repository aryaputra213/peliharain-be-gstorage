package eight.peliharain;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PeliharainApplication {


	public static void main(String[] args) {
		SpringApplication.run(PeliharainApplication.class, args);
	}

}
