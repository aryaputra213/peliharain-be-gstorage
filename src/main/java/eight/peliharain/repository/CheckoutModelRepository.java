package eight.peliharain.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import eight.peliharain.model.CheckoutModel;

public interface CheckoutModelRepository extends JpaRepository<CheckoutModel, Long>{  
}
