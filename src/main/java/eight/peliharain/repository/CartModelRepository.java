package eight.peliharain.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import eight.peliharain.model.CartModel;

public interface CartModelRepository extends JpaRepository<CartModel, Long>{  
}
