package eight.peliharain.repository;

import eight.peliharain.model.PostModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface PostModelRepository extends JpaRepository<PostModel, Long> {
    Optional<PostModel> findById(long id);
}
