package eight.peliharain.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import eight.peliharain.model.CheckoutProductQuantity;
import eight.peliharain.model.CheckoutProductQuantityID;

public interface CheckoutProductQuantityRepository extends JpaRepository<CheckoutProductQuantity, CheckoutProductQuantityID>{  
}