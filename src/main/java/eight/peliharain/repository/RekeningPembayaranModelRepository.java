package eight.peliharain.repository;

import eight.peliharain.model.RekeningPembayaranModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RekeningPembayaranModelRepository extends JpaRepository<RekeningPembayaranModel, Long> {
    // Checkme: pastiin lagi id buat rekening itu norek apa beda sendiri
    Optional<RekeningPembayaranModel> findById(long idRekening);
}
