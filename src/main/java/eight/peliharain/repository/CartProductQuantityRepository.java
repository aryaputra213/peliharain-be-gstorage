package eight.peliharain.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import eight.peliharain.model.CartProductQuantity;
import eight.peliharain.model.CartProductQuantityID;

public interface CartProductQuantityRepository extends JpaRepository<CartProductQuantity, CartProductQuantityID>{ 
    Optional<CartProductQuantity> findById(CartProductQuantityID cartProductQuantityID);
}