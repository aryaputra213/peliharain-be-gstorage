package eight.peliharain.repository;

import eight.peliharain.model.ProdukModel;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface ProdukModelRepository extends JpaRepository<ProdukModel, Long> {
    Optional<ProdukModel> findById(long id);
}
