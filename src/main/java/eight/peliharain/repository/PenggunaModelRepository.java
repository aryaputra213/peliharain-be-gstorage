package eight.peliharain.repository;

import eight.peliharain.model.PenggunaModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface PenggunaModelRepository extends JpaRepository<PenggunaModel, Long> {
    Optional<PenggunaModel> findByUsername(String username);
}
