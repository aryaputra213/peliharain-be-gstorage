package eight.peliharain.repository;

import eight.peliharain.model.TokoModel;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface TokoModelRepository extends JpaRepository<TokoModel, Long> {
    TokoModel findById(long id);
}
