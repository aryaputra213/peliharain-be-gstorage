package eight.peliharain.utils;

import eight.peliharain.model.ProdukModel;
import eight.peliharain.model.TokoModel;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public interface Sorter {
    public List<ProdukModel> filterAction(List<ProdukModel> ProdukModelList);
}
