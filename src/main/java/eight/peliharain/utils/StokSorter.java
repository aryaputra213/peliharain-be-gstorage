package eight.peliharain.utils;

import eight.peliharain.model.ProdukModel;

import java.util.List;

public class StokSorter implements Sorter{
    @Override
    public List<ProdukModel> filterAction(List<ProdukModel> produkModelList) {
        for (var i = 0 ; i < produkModelList.size() ; i++) {
            for (var j = 0 ; j < i ; j++ ) {
                if (produkModelList.get(i).getStok() < (produkModelList.get(j).getStok())) {
                    ProdukModel temp = produkModelList.get(i);
                    produkModelList.set(i, produkModelList.get(j));
                    produkModelList.set(j, temp);
                }
            }
        }
        return produkModelList;
    }

}
