package eight.peliharain.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.ManyToMany;
import javax.persistence.JoinTable;
import javax.persistence.JoinColumn;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Set;

@Data
@Getter
@Setter
@EqualsAndHashCode
@NoArgsConstructor
@Entity(name = "pengguna")
public class PenggunaModel extends UserModel implements Serializable {

    @Column
    private String namaLengkap;

    @Column(unique = true)
    private String username;

    @Column
    private HashMap<String, String> socialMedia;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "toko_id", referencedColumnName = "id")
    private TokoModel toko;

    @OneToMany(mappedBy = "penggunaModel")
    @JsonIgnore
    private Set<PostModel> postModelList;

    @ManyToMany
    @JsonIgnore
    @JoinTable(name = "bookmark", joinColumns = @JoinColumn(name = "pengguna"), inverseJoinColumns = @JoinColumn(name = "post"))
    Set<PostModel> bookmarks;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "cart_id", referencedColumnName = "id")
    @EqualsAndHashCode.Exclude
    private CartModel cart = new CartModel();

    @OneToMany(mappedBy = "pembeli")
    @EqualsAndHashCode.Exclude
    private Set<CheckoutModel> checkouts;

    // @OneToMany(mappedBy = "pemilikToko")
    // private Set<CheckoutModel> checkoutAsPemilikToko;
}
