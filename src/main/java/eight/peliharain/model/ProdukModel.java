package eight.peliharain.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@Entity(name = "produk")
public class ProdukModel implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(nullable = false)
    private String namaProduk;

    @Column(nullable = false)
    private String deskripsiProduk;

    @Column(nullable = false)
    private String foto;

    @Column(nullable = false)
    private int stok;

    @Column(nullable = false)
    private int harga;

    @Column
    private long idToko;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "tokoModel")
    private TokoModel tokoModel;

    @OneToMany(mappedBy = "produk")
    @JsonManagedReference
    private Set<CartProductQuantity> cartProductQuantities = new HashSet<>();

    @JsonIgnore
    @OneToMany(mappedBy = "produk")
    private Set<CheckoutProductQuantity> checkoutProductQuantities = new HashSet<>();

}
