package eight.peliharain.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import eight.peliharain.nonentity.Bank;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Setter
@Getter
@NoArgsConstructor
@Entity(name = "rekening_pembayaran")
public class RekeningPembayaranModel implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    // Checkme: pastiin lagi id buat rekening itu norek apa beda sendiri
    @Column(unique = true, nullable = false)
    private String nomorRekening;

    @Column
    private String qrCode;

    // Checkme: iseng bikin kan masa rekening gaada bank nya
    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name = "nama", column = @Column(name = "namaBank", nullable = false)),
            @AttributeOverride(name = "logo", column = @Column(name = "logoBank"))
    })
    private Bank bank;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "tokoModel")
    private TokoModel tokoModel;

}
