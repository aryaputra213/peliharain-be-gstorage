package eight.peliharain.model;

import lombok.AllArgsConstructor;  
import lombok.Data;  
import lombok.NoArgsConstructor;

import javax.persistence.Column;  
import javax.persistence.Embeddable;  
import java.io.Serializable;

@Data @AllArgsConstructor @NoArgsConstructor
@Embeddable
public class CheckoutProductQuantityID implements Serializable {  
    @Column(name = "checkout_id")
    private long checkout_id;

    @Column(name = "produk_id")
    private long produk_id;
}