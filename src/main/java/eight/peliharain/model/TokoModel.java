package eight.peliharain.model;

import eight.peliharain.nonentity.Alamat;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;
import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode
@Entity(name = "toko")
public class TokoModel implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(nullable = false)
    private String namaToko;

    @Column(nullable = false)
    private String deskripsiToko;

    @Column
    private String photo;

    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name = "alamat", column = @Column(nullable = false)),
            @AttributeOverride(name = "kota", column = @Column(nullable = false)),
            @AttributeOverride(name = "kodepos", column = @Column(nullable = false))
    })
    private Alamat alamat;


    @OneToMany(mappedBy = "tokoModel")
    private Set<ProdukModel> produkModelSet;


    @OneToMany(mappedBy = "toko")
    @EqualsAndHashCode.Exclude
    private Set<CheckoutModel> checkouts;

    @OneToMany(mappedBy = "tokoModel")
    private Set<RekeningPembayaranModel> rekeningPembayaranModelSet;
}

