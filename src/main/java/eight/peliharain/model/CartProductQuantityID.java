package eight.peliharain.model;

import lombok.AllArgsConstructor;  
import lombok.Data;  
import lombok.NoArgsConstructor;

import javax.persistence.Column;  
import javax.persistence.Embeddable;  
import java.io.Serializable;

@Data @AllArgsConstructor @NoArgsConstructor
@Embeddable
public class CartProductQuantityID implements Serializable {  
    @Column(name = "cart_id")
    private long card_id;

    @Column(name = "produk_id")
    private long produk_id;
}