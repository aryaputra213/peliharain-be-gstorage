package eight.peliharain.model;

import lombok.Getter;  
import lombok.NoArgsConstructor;  
import lombok.Setter;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Getter @Setter @NoArgsConstructor
@Entity
@Table(name = "checkout_product_quantity")
public class CheckoutProductQuantity {  
    @EmbeddedId
    private CheckoutProductQuantityID id;

    @ManyToOne
    @JsonIgnore
    @MapsId("checkoutId")
    @JoinColumn(name = "checkout_id")
    private CheckoutModel checkout;

    @ManyToOne
    @MapsId("produkId")
    @JoinColumn(name = "produk_id")
    private ProdukModel produk;

    @Column(name = "quantity")
    private Integer quantity;

    public CheckoutProductQuantity(CheckoutModel checkout, ProdukModel produk, Integer quantity) {
        this.id = new CheckoutProductQuantityID(checkout.getId(), produk.getId());
        this.checkout = checkout;
        this.produk = produk;
        this.quantity = quantity;
    }
}
