package eight.peliharain.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "cart_product_quantity")
public class CartProductQuantity {
    @EmbeddedId
    private CartProductQuantityID id;

    @ManyToOne
    @JsonIgnore
    @MapsId("cartId")
    @JoinColumn(name = "cart_id")
    private CartModel cart;

    @ManyToOne
    @MapsId("produkId")
    @JoinColumn(name = "produk_id")
    @JsonBackReference
    private ProdukModel produk;

    @Column(name = "quantity")
    private Integer quantity;

    public CartProductQuantity(CartModel cart, ProdukModel produk, Integer quantity) {
        this.id = new CartProductQuantityID(cart.getId(), produk.getId());
        this.cart = cart;
        this.produk = produk;
        this.quantity = quantity;
    }
}
