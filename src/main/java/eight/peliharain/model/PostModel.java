package eight.peliharain.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@Entity(name = "post")
public class PostModel implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(nullable = false)
    private String namaHewan;

    @Column(nullable = false)
    private String fotoHewan;

    @Column(nullable = false)
    private String deskripsi;

    @Column(nullable = false)
    private String domisili;

    @Enumerated(EnumType.STRING)
    @Column
    private StatusAdopsi status;

    @ManyToOne
    @JoinColumn(name = "penggunaModel")
    private PenggunaModel penggunaModel;

    @ManyToMany(mappedBy = "bookmarks")
    @JsonIgnore
    Set<PenggunaModel> booked_username;
}
