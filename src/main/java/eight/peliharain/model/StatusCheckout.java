package eight.peliharain.model;

public enum StatusCheckout {
    PROSES,
    TIDAK_TERSEDIA,
    SETUJU
}