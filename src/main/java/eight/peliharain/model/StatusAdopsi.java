package eight.peliharain.model;

public enum StatusAdopsi {
    MENUNGGU_VERIFIKASI,
    AKTIF,
    GAGAL_VERIFIKASI,
    TERADOPSI
}