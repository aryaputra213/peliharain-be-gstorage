package eight.peliharain.model;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.HashSet;
import java.util.Set;

@Data
@Getter
@Setter
@NoArgsConstructor
@Entity(name = "checkout")
public class CheckoutModel {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private long id;

    @OneToMany(mappedBy = "checkout", cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<CheckoutProductQuantity> checkoutProductQuantities = new HashSet<>();

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "pembeli")
    private PenggunaModel pembeli;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "toko")
    private TokoModel toko;

    @Enumerated(EnumType.STRING)
    @Column
    private StatusCheckout status;

    @Column(nullable = true)
    private String buktiPembayaran;

    @Column(nullable = true)
    private String deskripsi;

    @Column(nullable = true)
    private int jumlah;

    public CheckoutModel(
            PenggunaModel pembeli,
            TokoModel toko,
            StatusCheckout status,
            String deskripsi
    ) {
        this.pembeli = pembeli;
        this.toko = toko;
        this.status = status;
        this. deskripsi = deskripsi;
    }
}