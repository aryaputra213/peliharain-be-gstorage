package eight.peliharain.controller;

import eight.peliharain.model.CartProductQuantity;
import eight.peliharain.model.CartProductQuantityID;
import eight.peliharain.model.CheckoutModel;
import eight.peliharain.model.PostModel;
import eight.peliharain.services.CartService;
import eight.peliharain.services.PostService;
import lombok.AllArgsConstructor;

import java.util.ArrayList;
import java.util.Map;
import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/cart")
@AllArgsConstructor
public class CartController {

    @Autowired
    private CartService cartService;

    // @PostMapping(path = "/{username}", produces = {"application/json"})
    // @ResponseBody
    // public ResponseEntity createPost(@PathVariable(value = "username") String
    // username, @RequestBody Set<String> postModel) {
    // return ResponseEntity.ok(postService.createPost(username, postModel));
    // }

    @GetMapping(path = "/{username}", produces = { "application/json" })
    @ResponseBody
    public ResponseEntity getPostById(@PathVariable(value = "username") String username) {
        try {
            ArrayList<Map<Object, Object>> result = cartService.getProduk(username);
            if (result == null) {
                return new ResponseEntity(HttpStatus.FORBIDDEN);
            }

            return ResponseEntity.ok(result);

        } catch (Exception ex) {
            return new ResponseEntity(HttpStatus.FORBIDDEN);
        }
    }

    @PutMapping(path = "set-quantity/{username}/{quantity}", produces = { "application/json" })
    @ResponseBody
    public ResponseEntity setQuantity(@PathVariable(value = "username") String username,
            @PathVariable(value = "quantity") int quantity, @RequestBody CartProductQuantityID cartProductQuantityID) {
        try {
            Integer result = cartService.setQuantityProduk(username, quantity, cartProductQuantityID);
            if (result == null) {
                return new ResponseEntity(HttpStatus.FORBIDDEN);
            }

            return ResponseEntity.ok(result);

        } catch (Exception ex) {
            return new ResponseEntity(HttpStatus.FORBIDDEN);
        }
    }

    @PostMapping(path = "add-produk/{username}/{produkId}", produces = { "application/json" })
    @ResponseBody
    public ResponseEntity addItem(@PathVariable(value = "username") String username,
            @PathVariable(value = "produkId") long produkId) {
        try {
            CartProductQuantity result = cartService.addProdukToCart(username, produkId);
            if (result == null) {
                return new ResponseEntity(HttpStatus.FORBIDDEN);
            }

            return ResponseEntity.ok(result);

        } catch (Exception ex) {
            return new ResponseEntity(HttpStatus.FORBIDDEN);
        }
    }

    @DeleteMapping(path = "delete-produk/{username}/{idProduk}", produces = { "application/json" })
    @ResponseBody
    public ResponseEntity deleteItem(@PathVariable(value = "username") String username,
            @PathVariable(value = "idProduk") long idProduk) {
        try {
            cartService.removeProdukFromCart(username, idProduk);
            return ResponseEntity.ok(idProduk);
        } catch (Exception ex) {
            return new ResponseEntity(HttpStatus.FORBIDDEN);
        }
    }

    @PostMapping(path = "create-checkout/{username}/{description}", produces = { "application/json" })
    @ResponseBody
    public ResponseEntity createCheckout(@PathVariable(value = "username") String username,
            @PathVariable(value = "description") String deskripsi,
            @RequestBody ArrayList<CartProductQuantityID> cartProductQuantityIDList) {
        CheckoutModel result = cartService.createCheckoutFromCart(username, deskripsi, cartProductQuantityIDList);
        return ResponseEntity.ok(result);
    }

    @GetMapping(path = "get-cart-id/{username}", produces = { "application/json" })
    @ResponseBody
    public ResponseEntity getCartId(@PathVariable(value = "username") String username) {
        return ResponseEntity.ok(cartService.getCartId(username));
    }

    @GetMapping(path = "get-checkout/{username}", produces = { "application/json" })
    @ResponseBody
    public ResponseEntity getCheckout(@PathVariable(value = "username") String username) {
        try {
            Set<CheckoutModel> result = cartService.getCheckout(username);
            if (result == null) {
                return new ResponseEntity(HttpStatus.FORBIDDEN);
            }

            return ResponseEntity.ok(result);

        } catch (Exception ex) {
            return new ResponseEntity(HttpStatus.FORBIDDEN);
        }
    }
}
