package eight.peliharain.controller;

import eight.peliharain.model.ProdukModel;
import eight.peliharain.model.RekeningPembayaranModel;
import eight.peliharain.model.TokoModel;
import eight.peliharain.services.TokoServiceImpl;
import lombok.AllArgsConstructor;
import org.apache.coyote.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@RestController
@RequestMapping(path = "/")
@AllArgsConstructor
public class TokoController {

    @Autowired
    private TokoServiceImpl tokoServiceImpl;

    // Method untuk Toko
    @PostMapping(path = "/bukatoko/{usernamePengguna}" ,produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Object> createToko(@RequestBody TokoModel tokoModel,
                                             @PathVariable(value = "usernamePengguna") String usernamePengguna)
    {
        return ResponseEntity.ok(tokoServiceImpl.createToko(tokoModel, usernamePengguna));
    }

    @PutMapping(path = "/toko/{idToko}" ,produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Object> updateToko(@RequestBody TokoModel tokoModel, @PathVariable(value = "idToko") long idToko)
    {
        return ResponseEntity.ok(tokoServiceImpl.updateToko(tokoModel, idToko));
    }

    @GetMapping(path = "/toko/{idToko}",produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Object> getTokoById(@PathVariable(value = "idToko") long idToko) {
        return ResponseEntity.ok(tokoServiceImpl.getTokoById(idToko));
    }

    @DeleteMapping(path = "/toko/{idToko}" ,produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Object> deleteToko(@PathVariable(value = "idToko") long idToko)
    {
        return ResponseEntity.ok(tokoServiceImpl.deleteToko(idToko));
    }

    // Method untuk Produk
    @GetMapping(path = "/produk/{idProduk}",produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Object> getProdukById(@PathVariable(value = "idProduk") long idProduk) {
        return ResponseEntity.ok(tokoServiceImpl.getProdukById(idProduk));
    }

    @GetMapping(path = "/produk", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Object> getAllProduk() {
        return ResponseEntity.ok(tokoServiceImpl.getAllProdukModel());
    }

    @PostMapping(path = "/toko/produk/{idToko}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Object> addProduct(@RequestParam("file") MultipartFile file, @PathVariable(value = "idToko") long idToko , @ModelAttribute ProdukModel produkModel) throws IOException {
        System.out.println("CONTROLLER");
        return ResponseEntity.ok(tokoServiceImpl.addProduk(file, produkModel,idToko));
    }

    @PutMapping(path = "/toko/produk/{idProduk}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Object> updateProduct(@RequestBody ProdukModel produkModel,
                                                @PathVariable(value = "idProduk") long idProduk) {
        return ResponseEntity.ok(tokoServiceImpl.updateProduk(produkModel,idProduk));
    }

    @DeleteMapping(path = "/toko/produk/{idProduk}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Object> deleteProduct(@PathVariable(value = "idProduk") long idProduk) {
        return  ResponseEntity.ok(tokoServiceImpl.deleteProduk(idProduk));
    }

    @PostMapping(path = "/produk/sort/{kode}",produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Object> sortProduk(@PathVariable(value = "kode") long kode) {
        return ResponseEntity.ok(tokoServiceImpl.sortProduk(kode,-1));
    }

    @PostMapping(path = "/toko/produk/sort/{kode}/{idToko}",produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Object> sortProdukMilikToko(@PathVariable(value = "kode") long kode, @PathVariable(value = "idToko") long idToko) 
    {
        return ResponseEntity.ok(tokoServiceImpl.sortProduk(kode,idToko));
    }
    // Checkme: pastiin lagi id buat rekening itu norek apa beda sendiri
    // Method untuk Pembayaran
    @PostMapping(path = "/toko/rekening/{idToko}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Object> createRekening(@RequestBody RekeningPembayaranModel rekeningPembayaranModel,
                                                 @PathVariable(value = "idToko") long idToko)
    {
        return ResponseEntity.ok(tokoServiceImpl.createRekening(rekeningPembayaranModel, idToko));
    }

    @PutMapping(path = "/toko/rekening/{idRekening}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Object> updateRekening(@RequestBody RekeningPembayaranModel rekeningPembayaranModel,
                                                 @PathVariable(value = "idRekening") long idRekening)
    {
        return ResponseEntity.ok(tokoServiceImpl.updateRekening(rekeningPembayaranModel, idRekening));
    }

    @GetMapping(path = "/toko/rekening/{idRekening}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Object> getRekeningByNoRekening(@PathVariable(value = "idRekening") long idRekening)
    {
        return ResponseEntity.ok(tokoServiceImpl.getRekeningByNoRekening(idRekening));
    }

    @GetMapping(path = "/toko/rekening/", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Object> getAllRekening() {
        return ResponseEntity.ok(tokoServiceImpl.getAllRekening());
    }

    @DeleteMapping(path = "/toko/rekening/{idRekening}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Object> deleteRekening(@PathVariable(value = "idRekening") long idRekening) {
        return ResponseEntity.ok(tokoServiceImpl.deleteRekening(idRekening));
    }
}
