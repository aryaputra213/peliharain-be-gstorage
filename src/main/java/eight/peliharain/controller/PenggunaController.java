package eight.peliharain.controller;

import eight.peliharain.model.PenggunaModel;
import eight.peliharain.services.PenggunaServiceImpl;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/")
@AllArgsConstructor
public class PenggunaController {

    @Autowired
    private PenggunaServiceImpl penggunaServiceImpl;

    @GetMapping(path = "/pengguna/{username}",produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Object> getPenggunaAuth(@PathVariable(value = "username") String username) {
        return ResponseEntity.ok(penggunaServiceImpl.getPenggunaByUsername(username));
    }

    @PutMapping(path = "/pengguna/{username}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Object> updatePengguna(@PathVariable(value = "username") String username,
                                                 @RequestBody PenggunaModel penggunaModel) {
        return ResponseEntity.ok(penggunaServiceImpl.updateDataPengguna(username,penggunaModel));
    }

    @GetMapping(path = "/{username}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Object> getPengguna(@PathVariable(value = "username") String username) {
        return ResponseEntity.ok(penggunaServiceImpl.getPenggunaByUsername(username));
    }
}
