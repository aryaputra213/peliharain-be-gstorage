package eight.peliharain.controller;


import eight.peliharain.model.PenggunaModel;
import eight.peliharain.model.UserModel;
import eight.peliharain.services.AuthService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/auth")
@AllArgsConstructor
public class AuthController {

    @Autowired
    private AuthService authenticationService;

    @PostMapping(path = "/register")
    public ResponseEntity<Object> registerUser(@RequestBody PenggunaModel newUser) {
        return ResponseEntity.ok(authenticationService.registerUser(newUser));
    }
}
