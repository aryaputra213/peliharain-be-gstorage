package eight.peliharain.controller;

import eight.peliharain.model.PostModel;
import eight.peliharain.services.PostService;
import lombok.AllArgsConstructor;

import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/post")
@AllArgsConstructor
public class PostController {

    @Autowired
    private PostService postService;

    @PostMapping(path = "/{username}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity createPost(@PathVariable(value = "username") String username, @RequestBody PostModel postModel) {
        return ResponseEntity.ok(postService.createPost(username, postModel));
    }

    @GetMapping(produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Iterable<PostModel>> getAllPost() {
        return ResponseEntity.ok(postService.getAllPost());
    }

    @GetMapping(path = "/{idPost}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity getPostById(@PathVariable(value = "idPost") long idPost) {
        PostModel result = postService.getPostById(idPost);

        if (result == null) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }

        return ResponseEntity.ok(result);
    }

    @GetMapping(path = "/user/{username}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Iterable<PostModel>> getPostFromUser(@PathVariable(value = "username") String username) {
        Iterable<PostModel> result = postService.getPostsFromUser(username);

        if (result == null) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }

        return ResponseEntity.ok(result);
    }

    @PostMapping(path = "adopted/{username}/{idPost}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity markAdoptedPost(@PathVariable(value = "username") String username, @PathVariable(value = "idPost") long idPost) {
        try {
            PostModel result = postService.markAdoptedPost(username, idPost);

            if (result == null) {
                return new ResponseEntity(HttpStatus.NOT_FOUND);
            }

            return ResponseEntity.ok(result);

        } catch (Exception ex) {
            return new ResponseEntity(HttpStatus.FORBIDDEN);
        }
    }

    @GetMapping(path = "verification/{username}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity getAwaitingConfirmationPosts(@PathVariable(value = "username") String username) {
        try {
            Iterable<PostModel> result = postService.getAwaitingConfirmationPosts(username);

            if (result == null) {
                return new ResponseEntity(HttpStatus.NOT_FOUND);
            }

            return ResponseEntity.ok(result);

        } catch (Exception ex) {
            return new ResponseEntity(HttpStatus.FORBIDDEN);
        }
    }

    @PostMapping(path = "verify/{username}/{idPost}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity verifyPost(@PathVariable(value = "username") String username, @PathVariable(value = "idPost") long idPost) {
        try {
            PostModel result = postService.verifyPost(username, idPost);

            if (result == null) {
                return new ResponseEntity(HttpStatus.NOT_FOUND);
            }

            return ResponseEntity.ok(result);

        } catch (Exception ex) {
            return new ResponseEntity(HttpStatus.FORBIDDEN);
        }
    }

    @PostMapping(path = "deny/{username}/{idPost}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity denyPost(@PathVariable(value = "username") String username, @PathVariable(value = "idPost") long idPost) {
        try {
            PostModel result = postService.denyPost(username, idPost);

            if (result == null) {
                return new ResponseEntity(HttpStatus.NOT_FOUND);
            }

            return ResponseEntity.ok(result);

        } catch (Exception ex) {
            return new ResponseEntity(HttpStatus.FORBIDDEN);
        }
    }

    // Down here is bookmark code
    @PostMapping(path = "/add-bookmark/{username}/{idPost}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity addBookmark(@PathVariable(value = "username") String username, @PathVariable(value = "idPost") long idPost) {
        try {
            PostModel result = postService.addBookmark(username, idPost);

            if (result == null) {
                return new ResponseEntity(HttpStatus.FORBIDDEN);
            }

            return ResponseEntity.ok(result);

        } catch (Exception ex) {
            return new ResponseEntity(HttpStatus.FORBIDDEN);
        }
    }

    @DeleteMapping(path = "/remove-bookmark/{username}/{idPost}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity remove(@PathVariable(value = "username") String username, @PathVariable(value = "idPost") long idPost) {
        try {
            PostModel result = postService.removeBookmark(username, idPost);

            if (result == null) {
                return new ResponseEntity(HttpStatus.FORBIDDEN);
            }

            return ResponseEntity.ok(result);

        } catch (Exception ex) {
            return new ResponseEntity(HttpStatus.FORBIDDEN);
        }
    }

    @GetMapping(path = "/get-bookmarks/{username}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity getBookmarks(@PathVariable(value = "username") String username) {
        try {
            Set<PostModel> result = postService.getBookmarks(username);

            if (result == null) {
                return new ResponseEntity(HttpStatus.FORBIDDEN);
            }

            return ResponseEntity.ok(result);

        } catch (Exception ex) {
            return new ResponseEntity(HttpStatus.FORBIDDEN);
        }
    }

    @PostMapping(path = "/is-bookmarked/{username}/{idPost}", produces = {"application/json"})
    @ResponseBody
    public Boolean isBookmarked(@PathVariable(value = "username") String username, @PathVariable(value = "idPost") long idPost) {
        try {
            Boolean result = postService.isBookmarked(username, idPost);

            return result;

        } catch (Exception ex) {
            return null;
        }
    }
}
