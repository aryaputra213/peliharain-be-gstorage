package eight.peliharain.controller;

import eight.peliharain.model.CheckoutModel;
import eight.peliharain.model.PostModel;
import eight.peliharain.model.StatusCheckout;
import eight.peliharain.services.CheckoutServiceImpl;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/")
@AllArgsConstructor
public class CheckoutController {
    @Autowired
    CheckoutServiceImpl checkoutService;

    @GetMapping(path = "/checkout/status", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Object> getAllCheckout() {
        return ResponseEntity.ok(checkoutService.getAllCheckout());
    }

    @GetMapping(path = "/checkout/status/{username}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Object> getAllCheckoutPembeli(@PathVariable(value = "username") String username) {
        return ResponseEntity.ok(checkoutService.getCheckoutPembeli(username));
    }

    @GetMapping(path = "/checkout/store/{idToko}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Object> getAllCheckoutToko(@PathVariable(value = "idToko") long idToko) {
        return ResponseEntity.ok(checkoutService.getCheckoutToko(idToko));
    }

    @GetMapping(path = "/checkout/{id}",produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Object> getCheckoutById(@PathVariable(value = "id") long id) {
        return ResponseEntity.ok(checkoutService.getCheckoutById(id));
    }

    @PutMapping(path = "/checkout/update/{id}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Object> updateCheckout(@RequestBody CheckoutModel checkoutModel,
                                                @PathVariable(value = "id") long id) {
        return ResponseEntity.ok(checkoutService.updateCheckout(id,checkoutModel));
    }

    @PutMapping(path = "/checkout/updateStatus/{id}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Object> updateStatusCheckout(@RequestBody StatusCheckout statusCheckout,
                                                @PathVariable(value = "id") long id) {
        return ResponseEntity.ok(checkoutService.updateStatusCheckout(id,statusCheckout));
    }

    @DeleteMapping(path = "/remove-checkout/{id}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity remove(@PathVariable(value = "id") long id) {
        try {
            String result = checkoutService.deleteCheckout(id);

            if (result == null) {
                return new ResponseEntity(HttpStatus.FORBIDDEN);
            }

            return ResponseEntity.ok(result);

        } catch (Exception ex) {
            return new ResponseEntity(HttpStatus.FORBIDDEN);
        }
    }

}
